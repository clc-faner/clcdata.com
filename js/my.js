/**
 * Created by sheen on 2016/9/29.
 */
$(function () {
  //页脚
  (function () {
    $('.footer-right-content').find('.weixin').on('mouseenter', function () {
      $(this).addClass('current').find('.qrcode').css('display', 'block')
    }).on('mouseout', function () {
      $(this).removeClass('current').find('.qrcode').css('display', 'none');
    });
  })();

  //导航侧栏及swiper初始化
  (function () {
    $(".button-collapse").sideNav({menuWidth: 240, edge: 'right'});
    var swiper = new Swiper('.swiper-container', {
      loop: true,
      autoplay: 3000,
      autoplayDisableOnInteraction: false,
      pagination: '.swiper-pagination',
      paginationClickable: true
    });
  })();

  //折叠菜单
  $(".dropdown-button").dropdown();

  //折叠菜单
  (function () {
    $(document).ready(function () {
      $('.collapsible').collapsible();
    });
  })();

  //轮播图片
  (function () {
    $(document).ready(function () {
      $('.slider').slider();
    });
  })();

  //图片放大
  (function () {
    $(document).ready(function () {
      $('.materialboxed').materialbox();
    });
  });

  //ie兼容
  (function () {
    var isIE = function () { //ie?
      if (!!window.ActiveXObject || "ActiveXObject" in window)
        return true;
      else
        return false;
    };
    if (isIE()) {
      console.log('亲，还在用IE浏览器~ ')
      $('.logo-img').css('height', '1.5rem');
      $('html,body,.swiper-container,.swiper-wrapper,.swiper-slide').css('width', '100%');
    } else {
      return 0;
    }
    ;
  })();

  //移动端banner小图
  (function () {
    function resize() {
      var windowWidth = $(window).width();
      console.log(windowWidth);
      var isSmallScreen = windowWidth < 600;
      $('.swiper-wrapper>.swiper-slide').each(function (i, item) {
        var $item = $(item);
        var imgSrc = $item.data(isSmallScreen ? 'image-s' : 'image-l');
        if (isSmallScreen) {
          $(item).html('<img src="' + imgSrc + '" alt="small" class="responsive-img">');
        } else {
          $(item).html('<img src="' + imgSrc + '" alt="big" class="responsive-img">')
        }
      })
    }

    $(window).on('resize', resize).trigger('resize');
  })();

  //nav dropdown菜单
  //(function () {
  //  $('.dropdown1').on('mouseenter',function(){
  //    $('.dropdown1').find('.dropdown-content1').css('display','block');
  //  }).on('mouseout',function(){
  //    $(this).find('.dropdown-content1').css('display','none');
  //  })
  //})();


  //index-test页 nav变色
  //(function(){
  //  var search=function () {
  //    var sectionbigbannerbox = document.getElementsByClassName('section-bigbanner')[0];
  //    console.log(sectionbigbannerbox)
  //    var testnav = document.getElementsByClassName('nav-index-test')[0];
  //    var height = sectionbigbannerbox.offsetHeight;
  //    console.log('---------------'+height);
  //
  //    window.onscroll = function () {
  //      var top = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;;
  //      var op = top / height;
  //      console.log(op);
  //      if (top > height-56) {
  //        testnav.style.backgroundColor = "rgba(255,255,255,1)";
  //        $('.nav-index-test').css('box-shadow','0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)');
  //        $('nav').find('.right').find('a').css('color','#333');
  //        $('nav').find('.logo-img-head').attr('src','./images/logo-02.png');
  //        $('nav').find('i').addClass('md-dark');
  //      } else {
  //        testnav.style.backgroundColor = "rgba(255,255,255,"+op/2+")";
  //        $('.nav-index-test').css('box-shadow','0 2px 5px 0 rgba(0,0,0,0),0 2px 10px 0 rgba(0,0,0,0)');
  //        $('nav').find('.right').find('a').css('color','#fff');
  //        $('nav').find('.logo-img-head').attr('src','./images/logo-01.png');
  //        $('nav').find('i').removeClass('md-dark');
  //        //testnav.style.background = "rgba(255,255,255)";
  //      }
  //    }
  //  };
  //  search();
  //})();
});

function autoPlayMusic() {
  /* 自动播放音乐效果，解决浏览器或者APP自动播放问题 */
  function musicInBrowserHandler() {
    musicPlay(true);
    document.body.removeEventListener('touchstart', musicInBrowserHandler);
  }

  document.body.addEventListener('touchstart', musicInBrowserHandler);
  /* 自动播放音乐效果，解决微信自动播放问题 */
  function musicInWeixinHandler() {
    musicPlay(true);
    document.addEventListener("WeixinJSBridgeReady", function () {
      musicPlay(true);
    }, false);
    document.removeEventListener('DOMContentLoaded', musicInWeixinHandler);
  }

  document.addEventListener('DOMContentLoaded', musicInWeixinHandler);
}
function musicPlay(isPlay) {
  var media = document.getElementById('myMusic');
  if (isPlay && media.paused) {
    media.play();
  }
  if (!isPlay && !media.paused) {
    media.pause();
  }
};

//function menuFix(){
//  var sfEls = document.getElementById("nav").getElementsByTagName("li");
//  for (var i=0; i<sfEls.length; i++){
//    sfEls[i].onmouseover=function(){
//      this.className+=(this.className.length>0? " ": "") + "sfhover";
//    }
//    sfEls[i].onMouseDown=function(){
//      this.className+=(this.className.length>0? " ": "") + "sfhover";
//    }
//    sfEls[i].onMouseUp=function(){
//      this.className+=(this.className.length>0? " ": "") + "sfhover";
//    }
//    sfEls[i].onmouseout=function(){
//      thisthis.className=this.className.replace(new RegExp("( ?|^)sfhover\\b"),
//        "");
//    }
//  }
//}
//window.onload=menuFix;
