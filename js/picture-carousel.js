/**
 * Created by faner on 2017/6/6.图片轮播
 */
$(function(){
  //思路很重要 先要想好这个案例里面需要我们做的事情
  //1.左右按钮点击可以切换 2.定时器可以切换 3.点击ol的li也可以切换
  //1.1右按钮和定时器的代码一样 1.2左按钮单独控制
  //1.1.1 我们需要两个全局变量才能做无缝滚动效果，变量1：控制ol的索引值 变量2：控制ul的li的索引值（因为无缝滚动需要“临时工” 所以ul的li要多一个）1.1.2控制极值(olKey的最大是3 imgKey最大值是4)
  //1.2.1 左按钮的极值点是0 ，但是要恢复的位置是-1600px、imgKey = 3;//因为最后一张图是临时工 我们不能把它算进去 所以最后应该用3去判断最后一个值
  //2.1定时器 只要写成跟右按钮一样的代码即可， 既然是代码一样 我们可以设置变量去存储
  //3.1 ol的li被点击的时候 别忘了控制全局变量的两个值都变成$(this).index(),因为4个ol的li对应五个ul的li不会出现bug


  //第一件事情：为了实现无缝滚动效果 一定要请临时工 个数就是1
  //定义全局变量的时候 尽量写在js代码的最上面 ：目的是写在一起的话 一下就能看出来变量是否冲突 全局变量冲突在js中有一个解释叫做：全局变量污染
  var myLinShi = $('.con ul li').eq(0).clone(true);
  var myTag = $(myLinShi);
  var olLiKey = 0;//初始值为0 因为第0张图是当前的图片 还因为ol的li个数和ul的li个数不相等了 所以一个变量是不够的
  var imgKey = 0;//这个变量存的是img图片的索引值
  //定时器
  var timer01 = null;
  function myFn(){
    /* Act on the event */
    olLiKey++;
    if(olLiKey > 4){
      olLiKey = 0;
    }
    //ol的li变化
    $('.con ol li').eq(olLiKey).addClass('current').siblings().removeClass('current');

    //ul的变化
    imgKey++;
    //这里的极值判断：因为li有6个 所以我们imgkey的极值是不能大于5
    if(imgKey > 5){
      //因为最后一个图是；临时工 用户以为看到的是第0张图 所以我们要让点击之后 判断的极值点回复到编号1的图才可以
      imgKey = 1;
      //为了实现无缝滚动 一定要让ul一瞬间回到0的位置  然后再控制animate跑到编号1的图片位置（-900） 0到-900就是移动一个图片的距离 这就实现了无缝滚动
      $('.con ul').css('left','0px');
    }
    var imgWeiZhi = imgKey * -900;//想看到的那张图所在的位置信息
    $('.con ul').stop().animate({'left':imgWeiZhi + 'px'},500);
  }

  timer01 = setInterval(function(){
    myFn();
  },1000);


  $('.con ul').append(myTag);
  //写完这句话之后 ul标签的宽度别忘了修改 6*900
  $('.con .rightBtn').click(function(event) {
    myFn();
  });
  $('.con .leftBtn').click(function(event) {
    /* Act on the event */
    //ol的li
    olLiKey--;
    if(olLiKey < 0){
      olLiKey = 4;
    }
    $('.con ol li').eq(olLiKey).addClass('current').siblings().removeClass('current');
    //控制ul切换
    imgKey--;
    if(imgKey < 0){
      imgKey = 4;
      $('.con ul').css('left','-4500px');
    }
    var imgWeiZhi = imgKey * -900;
    $('.con ul').stop().animate({'left':imgWeiZhi + 'px'},500);
  });

  //ol的li点击事件
  $('.con ol li').click(function(event) {
    /* Act on the event */
    //为了防止点击li之后 图片切换顺序出现问题 我们必须要把当前点击的索引值赋值给imgkey和olkey 这就是改变了全局变量的值
    imgKey = $(this).index();
    olLiKey = $(this).index();
    $('.con ol li').eq($(this).index()).addClass('current').siblings().removeClass('current');
    var imgWeiZhi = imgKey * -900;
    $('.con ul').stop().animate({'left':imgWeiZhi + 'px'},500);
  });
  //滑过con停止定时器 划出再开启
  $('.con').hover(function() {
    clearInterval(timer01);
  }, function() {
    timer01 = setInterval(function(){
      myFn();
    },1000);
  });
});
